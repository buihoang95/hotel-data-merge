import re

GENERAL_AMENITIES = ["outdoor pool", "indoor pool", "business center",
                    "childcare", "wifi", "dry cleaning", "breakfast",
                    "outdoor pool","parking","bar","dry cleaning","concierge"]
ROOM_AMENITIES = ["aircon", "tv", "coffee machine", "kettle", "hair dryer", "iron", "bathtub","minibar"]


def normalize_amenities(s):
    s = s.strip(" ")
    s = re.sub("([a-z])([A-Z])","\g<1> \g<2>",s)
    s = s.lower()
    return s


def longer_len(s1, s2):
    if len(s1) > len(s2):
        return s1
    else: return s2
    

def hotel_id(hotels):
    for hotel in hotels:
        hotel_id = hotel.get_value_of("id")
        if hotel_id is None:
            continue
        
        return hotel_id
    
    return None

def destination_id(hotels):
    for hotel in hotels:
        destination_id = hotel.get_value_of("destination_id")
        if destination_id is None:
            continue
        
        return destination_id        
        
    return None


def name(hotels):
    result = ""
    for hotel in hotels:
        hotel_name = hotel.get_value_of("name")
        if hotel_name is None:
            continue
        
        result = longer_len(result, hotel_name)
        
    return result

def long(hotels):
    for hotel in hotels:
        long = hotel.get_value_of("long")
        if long is None:
            continue
        
        return long
        
    return None


def lat(hotels):
    for hotel in hotels:
        lat = hotel.get_value_of("lat")
        if lat is None:
            continue
        
        return lat
        
    return None


def address(hotels):
    result = ""
    for hotel in hotels:
        address = hotel.get_value_of("address")
        if address is None:
            continue
        
        result = longer_len(result, address)
            
    return result if result is not "" else None
    

def amenities_general(hotels):
    all_amenities = []
    for hotel in hotels:
        amenities = hotel.get_value_of("amenities_general")
        if amenities is not None:    
            all_amenities += amenities
    
    general_amenities = filter(lambda x: x in GENERAL_AMENITIES, all_amenities)
    
    return list(general_amenities)
        
            
def amenities_room(hotels):
    all_amenities = []
    for hotel in hotels:
        amenities = hotel.get_value_of("amenities_room")
        if amenities is not None:    
            all_amenities += amenities
        
    general_amenities = filter(lambda x: x in ROOM_AMENITIES, all_amenities)
    
    return list(general_amenities)


def city(hotels):
    result = ""
    for hotel in hotels:
        city = hotel.get_value_of("city")
        if city is None:
            continue
        
        result = longer_len(result, city)
        
    return result if result is not "" else None


def country(hotels):
    result = ""
    for hotel in hotels:
        country = hotel.get_value_of("country")
        
        if country is None:
            continue
            
        result = longer_len(result, country)
        
    return result if result is not "" else None


def description(hotels):
    result = ""
    for hotel in hotels:
        description = hotel.get_value_of("description")
        if description is None:
            continue
        
        result = longer_len(result, description)
        
    return result if result is not "" else None


def booking_condition(hotels):
    result = []
    for hotel in hotels:
        booking_condition = hotel.get_value_of("booking_condition")
        if booking_condition is None:
            continue
        
        result = longer_len(result, booking_condition)
        
    return result


def aggregate_images(images):
    url_to_caption = {}
    for image in images:
        url = image.get("link")
        if url is not None:
            url_to_caption[url] = image.get("caption")
        
        url = image.get("url")
        if url is not None:
            url_to_caption[url] = image.get("description")


    result = []
    for url, caption in url_to_caption.items():
        result.append({
            "link": url,
            "description": caption
        })
        
    return result


def images_rooms(hotels):
    image_rooms = []
    for hotel in hotels:
        image_room = hotel.get_value_of("images_rooms")
        if image_room is None:
            continue
        
        image_rooms += image_room

    result = aggregate_images(image_rooms)
        
    return result


def images_site(hotels):
    image_sites = []
    for hotel in hotels:
        image_site = hotel.get_value_of("images_site")
        if image_site is None:
            continue
        
        image_sites += image_site
        
    result = aggregate_images(image_sites)
    
    return result



def images_amenities(hotels):
    image_amenities = []
    for hotel in hotels:
        image_amenitie = hotel.get_value_of("images_amenities")
        if image_amenitie is None:
            continue
        
        image_amenities += image_amenitie
        
    result = aggregate_images(image_amenities)
    
    
    return result