ALIASES = {
    "id": ["Id", "hotel_id", "id"],
    "destination_id" : ["DestinationId", "destination_id", "destination"],
    "name": ["name", "hotel_name", "Name"],
    "amenities": ["amenities", "Facilities"],
    "amenities_general": ["amenities__general", "amenities", "Facilities"],
    "amenities_room": ["amenities__room", "amenities", "Facilities"],
    "long": ["Longitude", "lng"],
    "lat": ["Latitude", "lat"],
    "address": ["Address", "address", "location__address"],
    "city": ["City"],
    "country": ["Country", "location__country"],
    "description": ["details","Description","info"],
    "booking_condition": ["booking_conditions"],
    "images_rooms": ["images__rooms"],
    "images_site": ["images__site"],
    "images_amenities": ["images__amenities"] 
}

def get_alias(alias, keys):
    """
    Find alias in list of keys(), for example:
        - you want to find alias of "id" in list ["hotel_id", "bathrooms", "long","lat"]
        get_alias("id", ["hotel_id", "bathrooms", "long","lat"]) -> "hotel_id"
    """
    if alias not in ALIASES.keys():
        return None

    arr = ALIASES[alias]
    a = list(set(arr)&set(keys))
    
    if len(a) == 0:
        return None
    
    return a[0]


def get_alias_value(alias, hotel_info):
    if alias not in ALIASES.keys():
        return None
    
    aliases = ALIASES[alias]
    
    for alias in aliases:
        info = hotel_info
        keys = alias.split("__")
        
        for key in keys:
            info = info.get(key)
            if info is None or type(info) == list:
                break
            
        if info is not None:
            return info

    return info