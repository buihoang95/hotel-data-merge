import unittest

from alias import get_alias_value

hotel_info ={
        "Id": "iJhz",
        "DestinationId": 5432,
        "location":{
            "country": "Singapore"
        }
    }

class TestSum(unittest.TestCase):
    def test_get_alias_value_first_layer_return_value(self):
        """
        Test get alias of the first layer
        """
        actual = get_alias_value("id", hotel_info)
        expected = "iJhz"
        self.assertEqual(actual, expected)
    
    def test_get_alias_value_first_layer_return_none(self):
        """
        Test get alias of the first layer
        """
        actual = get_alias_value("image", hotel_info)
        expected = None
        self.assertEqual(actual, expected)
        
    def test_get_alias_value_deep_layer_return_value(self):
        """
        Test get alias of the first layer
        """
        actual = get_alias_value("country", hotel_info)
        expected = 'Singapore'
        self.assertEqual(actual, expected)
        
    def test_get_alias_value_deep_layer_return_none(self):
        """
        Test get alias of the first layer
        """
        actual = get_alias_value("city", hotel_info)
        expected = None
        self.assertEqual(actual, expected)

if __name__ == '__main__':
    unittest.main()
