import os
import yaml
import logging

def get_settings(path):
    return Settings(path)


# Open config file, check config local
def open_settings_file(name):
    main_file = name

    if os.path.isfile(main_file):
        return open(main_file)

    logging.info("not contain file")
    # logging.debug(os.path.dirname(os.path.abspath(__file__)))
    return None  # Take care if the file is not exists


class Settings:
    def __init__(self, path):
        self.path = path

    def __getitem__(self, name):
        return Item(self.path, name)


class Item:
    def __init__(self, path, name):
        self.path = path
        self.name = name

    def __getitem__(self, name):
        key = str(self.name + '__' + name)
        value = self.getEnv(name, None)
        if value is None:
            settings = yaml.safe_load(open_settings_file(self.path))
            value = settings[self.name][name]
        # else:
            # logging.info('value %s %s %s', self.name, name, value)
        return value

    def getEnv(self, name, default=None):
        key = str(self.name + '__' + name).upper()
        return os.environ.get(key, default)
