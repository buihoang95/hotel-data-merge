# -*- coding: utf-8 -*-
import logging
import sys
from datetime import datetime
from pythonjsonlogger import jsonlogger
import os

DEBUG = os.getenv("DEBUG_MODE", False)


class CTJsonFormatter(jsonlogger.JsonFormatter):
    def add_fields(self, log_record, record, message_dict):
        super(CTJsonFormatter, self).add_fields(log_record, record, message_dict)
        if not log_record.get('timestamp'):
            # this doesn't use record.created, so it is slightly off
            now = datetime.now().strftime('%d-%m-%YT%H:%M:%S.%fZ')
            log_record['timestamp'] = now
        if log_record.get('level'):
            log_record['level'] = log_record['level'].upper()
        # if not log_record.get('status_code'):
        #     log_record
        else:
            log_record['level'] = record.levelname


formatter = CTJsonFormatter('(timestamp) (level) (name) (message)')


def get_console_handler():
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(formatter)
    return console_handler


def get_logger(logger_name):
    logger = logging.getLogger(logger_name)

    logger.setLevel(logging.INFO)
    if DEBUG:
        logger.setLevel(logging.DEBUG)

    logger.addHandler(get_console_handler())

    logger.propagate = False

    return logger
