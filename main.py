import os
import json
import pickle
import requests

from bottle import route, request, static_file, run, response

from logger.log import get_logger
from config.settings import get_settings
from data.ascenda import Ascenda


config = get_settings(os.path.abspath("config/config.yaml"))

log = get_logger("main")
hotel_dataset = Ascenda()

@route('/hotel/<hotel_id>', method='GET')
def suggest(hotel_id):
    if hotel_id == "":
        response.status = 404
        return {"error": "invalid params"}
    
    response.status = 200
    hotel_info = hotel_dataset.get_hotel(hotel_id)
    if hotel_info is None:
        response.status = 404
        return {"error": "cannot get hotel_id {}".format(hotel_id)}
    
    response.content_type = 'application/json'
    return hotel_info

@route('/destination/<destination_id>', method='GET')
def suggest(destination_id):
    if destination_id == "":
        response.status = 404
        return {"error": "invalid params"}
    
    try:
        destination_id = int(destination_id)
    except Exception as e:
        response.status = 404
        return {"error": "invalid destination_id: {}".format(e)}
    
    hotels_info = hotel_dataset.get_hotel_by_destination(destination_id)
    if hotels_info is None:
        response.status = 404
        return {"error": "cannot get destination_id {}".format(destination_id)}
    
    response.status = 200
    response.content_type = 'application/json'
    return json.dumps(hotels_info)

if __name__=="__main__":
    run(host='0.0.0.0', port=8080, server="paste",reloader=True)