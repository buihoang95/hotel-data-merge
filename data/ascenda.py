import os
import requests
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from collections import defaultdict

from logger.log import get_logger
from config.settings import get_settings
from data.supplier import Supplier
from logics import aggregates
from utils import util

config = get_settings(os.path.abspath("config/config.yaml"))
log = get_logger("main")

class Ascenda():
    def __init__(self):
        supplier_sources = config["suppliers"]["url"].split(",")
        suppliers = list(map(Supplier, supplier_sources))
        self.suppliers = suppliers
        
        self.hotel_ids = []
        for supplier in self.suppliers:
            self.hotel_ids += supplier.get_all_hotel_ids()
        self.hotel_ids = list(set(self.hotel_ids))
        
        self.id_to_hotel = {}
        self.destination_to_hotels = defaultdict(list)
        
        for hotel_id in self.hotel_ids:
            hotels = [supplier.get_hotel_by_id(hotel_id) for supplier in self.suppliers]
            hotels = util.remove_none(hotels)
            merged = self.merge_hotels(hotels)
            self.id_to_hotel[hotel_id] = merged
            destination_id = merged["destination_id"]
            self.destination_to_hotels[destination_id].append(merged)
            
        return None


    def get_hotel(self, hotel_id):
        return self.id_to_hotel.get(hotel_id)


    def get_hotel_by_destination(self, des_id):
        return self.destination_to_hotels.get(des_id)


    def merge_hotels(self, hotels):
        # print(aggregates.amenities_general(hotels))
        
        hotel_info = {
            "id": aggregates.hotel_id(hotels),
            "destination_id": aggregates.destination_id(hotels),
            "name": aggregates.name(hotels),
            "location": {
                "lat": aggregates.lat(hotels),
                "lng": aggregates.long(hotels),
                "address": aggregates.address(hotels),
                "city": aggregates.city(hotels),
                "country": aggregates.country(hotels)
            },
            "description": aggregates.description(hotels),
            "amenities": {
                "general": aggregates.amenities_general(hotels),
                "room": aggregates.amenities_room(hotels)
            },
            "images": {
                "rooms": aggregates.images_rooms(hotels),
                "site": aggregates.images_site(hotels),
                "amenities": aggregates.images_amenities(hotels)
            },
            
            "booking_conditions": aggregates.booking_condition(hotels)
            
        }
    
        return hotel_info
        

# a = Ascenda()
# print(a.get_hotel("SjyX"))