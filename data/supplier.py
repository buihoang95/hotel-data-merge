import os
import requests
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from logger.log import get_logger
from config.settings import get_settings
from logics.alias import get_alias
from data.hotel import Hotel


config = get_settings(os.path.abspath("config/config.yaml"))
log = get_logger("main")

class Supplier():
    def __init__(self, url):
        response = requests.get(url)
        if response.status_code > 300:
            log.error("cannot get dataset from url {} with reason {}".format(url, response.content)
                      )
            return None
        
        hotels = response.json()
        
        self.hotels = list(map(Hotel, hotels))
        
        self.hotel_ids = []
        self.destination_ids = []
        self.id_to_hotel = {}
        self.process_hotels()
            
        return None
    
    
    def get_hotel_by_id(self, id):
        return self.id_to_hotel.get(id, None)
    
    
    def get_hotels_by_destination(self, id):
        return
    
    
    def get_all_hotel_ids(self):
        return self.hotel_ids
    
    
    def process_hotels(self):
        for hotel in self.hotels:
            hotel_id = hotel.get_value_of("id")
            self.hotel_ids.append(hotel_id)
            self.id_to_hotel[hotel_id] = hotel
            
            destination_id = hotel.get_value_of("destination_id")
            self.destination_ids.append(destination_id)
    
    
    
a = Supplier(config["suppliers"]["url"].split(",")[0])

