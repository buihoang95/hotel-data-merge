import os
import requests
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from logger.log import get_logger
from config.settings import get_settings
from logics.alias import *

log = get_logger("Hotel")

class Hotel():
    def __init__(self, hotel_info):
        self.hotel = hotel_info
        
    
    def get_value_of(self, key):
        return get_alias_value(key, self.hotel)