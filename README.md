# Ascenda Excercise - Hotel data merge

## I. Introduction
aggregate and serve merged hotel info from various data source

**Architecture**
```
  +----------+
  |          |
  |  Hotel   |
  |          |
  +----+-----+
       ^
       |contain
       |
       |
+------+-------+              +---------------+
|              |   aggregate  |               |
|  Supplier    +<-------------+   Ascenda     |
|              |              +               |
|              |              |               |
+--------------+              +---------------+
```
**solution**

* Aggregate string data base on their length. For example: city, country,...
* Amenities merged based on list of pre-defined available amenities
* Images are uniqued by their **url**
* See **./logics/ggregates.py** for aggregation logics
* See **./logics/alias.py** for data finding from supplier data source

## II. Prerequisites
* Python >= 3.5
* pip3 >= 10.0.1 for package management

## III. Usage
* **GET** /hotel/<hotel_id>
* **GET** /destination/<destination_id>

## IV. How to run localy
> pip3 install -r requirements.txt
> 
and 
>
> python3 main.py

## V. Build and deploy 
Prerequisites: [Docker](https://www.docker.com)

> docker build -f Dockerfile .

