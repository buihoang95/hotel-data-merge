def remove_none(arr):
    result = filter(lambda x: x is not None, arr)
    return list(result)